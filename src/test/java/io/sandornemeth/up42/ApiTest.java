package io.sandornemeth.up42;


import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import net.minidev.json.JSONArray;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Base64;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApiTest {

    @Autowired
    private TestRestTemplate testRestTemplate;

    @Test
    public void listsFeatures() {
        ResponseEntity<RetrievedFeature[]> response =
                testRestTemplate.getForEntity("/features", RetrievedFeature[].class);

        // check the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_UTF8);

        // and the payload
        RetrievedFeature[] features = response.getBody();
        assertThat(features).isNotEmpty();
        assertThat(features).hasSize(14);

        RetrievedFeature first = features[0];
        assertThat(first.getId()).isEqualTo("39c2f29e-c0f8-4a39-a98b-deed547d6aea");
        assertThat(first.getTimestamp()).isEqualTo(1554831167697L);
        assertThat(first.getBeginViewingDate()).isEqualTo(1554831167697L);
        assertThat(first.getEndViewingDate()).isEqualTo(1554831202043L);
        assertThat(first.getMissionName()).isEqualTo("Sentinel-1B");
    }

    @Test
    public void getsSingleFeature() {
        ResponseEntity<RetrievedFeature> response =
                testRestTemplate.getForEntity("/features/39c2f29e-c0f8-4a39-a98b-deed547d6aea", RetrievedFeature.class);

        // check the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.APPLICATION_JSON_UTF8);

        // and the payload
        RetrievedFeature first = response.getBody();
        assertThat(first.getId()).isEqualTo("39c2f29e-c0f8-4a39-a98b-deed547d6aea");
        assertThat(first.getTimestamp()).isEqualTo(1554831167697L);
        assertThat(first.getBeginViewingDate()).isEqualTo(1554831167697L);
        assertThat(first.getEndViewingDate()).isEqualTo(1554831202043L);
        assertThat(first.getMissionName()).isEqualTo("Sentinel-1B");
    }

    @Test
    public void returnsNotFoundWhenFeatureIsUnknown() {
        ResponseEntity<RetrievedFeature> response =
                testRestTemplate.getForEntity("/features/unknown", RetrievedFeature.class);

        // check the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void returns404WhenFeatureForQuicklookNotFound() {
        ResponseEntity<RetrievedFeature> response =
                testRestTemplate.getForEntity("/features/unknown/quicklook", RetrievedFeature.class);

        // check the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.NOT_FOUND);
    }

    @Test
    public void returnsImageWhenQuicklookIsUsed() {
        String id = "39c2f29e-c0f8-4a39-a98b-deed547d6aea";
        ResponseEntity<byte[]> response = testRestTemplate.getForEntity("/features/" + id + "/quicklook", byte[].class);

        // check the response
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getHeaders().getContentType()).isEqualTo(MediaType.IMAGE_PNG);

        assertThat(response.getBody()).isEqualTo(getExpectedImage(id));
    }

    private byte[] getExpectedImage(String id) {
        Object document = Configuration.defaultConfiguration().jsonProvider()
                .parse(this.getClass().getResourceAsStream("/source-data.json"), "UTF-8");
        String query = "$[*].features[0].properties[?(@.id == '" + id + "')].quicklook";
        System.out.println(query);
        JSONArray data = JsonPath.read(document, query);
        String binaryData = data.get(0).toString();
        return Base64.getDecoder().decode(binaryData);
    }


    static class RetrievedFeature {
        private String id;
        private long timestamp;
        private long beginViewingDate;
        private long endViewingDate;
        private String missionName;

        public String getId() {
            return id;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public long getBeginViewingDate() {
            return beginViewingDate;
        }

        public long getEndViewingDate() {
            return endViewingDate;
        }

        public String getMissionName() {
            return missionName;
        }
    }
}
