# Up42 coding challenge

This repository contains the coding challenge for [Up42](https://up42.com/).

## Build

The project is a standard Spring Boot web application built with Maven. 
To build the project, invoke the normal Maven package command: 

```bash
mvn package
```

If the tests should run without packaging, use:

```bash
mvn test
```

## Run

There are two ways to run the application (the commands assume that the
current directory is the root directory of this repository): 

1. Either using the package from the command line
   ```bash
   java -jar target/up42-coding-challenge-0.1.0.jar
   ```  
2. Or using the Maven Spring Boot plugin
   ```bash
   mvn spring-boot:run
   ```
   