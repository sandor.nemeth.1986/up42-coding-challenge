package io.sandornemeth.up42.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Properties {
    private String id;
    private long timestamp;
    private Acquisition acquisition;
    private byte[] quicklook;

    public String getId() {
        return id;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Acquisition getAcquisition() {
        return acquisition;
    }

    public byte[] getQuicklook() {
        return quicklook;
    }
}
