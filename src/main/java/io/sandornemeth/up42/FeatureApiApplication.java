package io.sandornemeth.up42;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Main entry point of the application.
 */
@SpringBootApplication
public class FeatureApiApplication {

    public static void main(String... args) {
        SpringApplication.run(FeatureApiApplication.class, args);
    }

}
