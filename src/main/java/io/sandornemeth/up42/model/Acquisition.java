package io.sandornemeth.up42.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Acquisition {
    private long beginViewingDate;
    private long endViewingDate;
    private String missionName;

    public long getBeginViewingDate() {
        return beginViewingDate;
    }

    public long getEndViewingDate() {
        return endViewingDate;
    }

    public String getMissionName() {
        return missionName;
    }
}
