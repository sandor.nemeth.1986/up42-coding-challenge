package io.sandornemeth.up42.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Feature {
    private Properties properties;

    public Properties getProperties() {
        return properties;
    }
}
