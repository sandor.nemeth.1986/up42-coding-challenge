package io.sandornemeth.up42;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Maps;
import io.sandornemeth.up42.model.Feature;
import io.sandornemeth.up42.model.FeatureCollection;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Repository
public class FeatureRepository {

    private static final String SOURCE_LOCATION = "classpath:/source-data.json";

    private final ObjectMapper objectMapper;
    private final ResourceLoader resourceLoader;

    private List<Feature> features;
    private Map<String, Feature> featureIndex;

    public FeatureRepository(ObjectMapper objectMapper, ResourceLoader resourceLoader) {
        this.objectMapper = objectMapper;
        this.resourceLoader = resourceLoader;
    }

    @PostConstruct
    public void initializeRepository() throws Exception {
        Resource source = resourceLoader.getResource(SOURCE_LOCATION);
        FeatureCollection[] featureCollections = objectMapper.readValue(source.getInputStream(), FeatureCollection[].class);
        features = Arrays.stream(featureCollections)
                .flatMap(collection -> collection.getFeatures().stream())
                .collect(toList());
        featureIndex = Maps.uniqueIndex(features, feature -> feature.getProperties().getId());
    }

    public List<Feature> findAll() {
        return features;
    }

    public Optional<Feature> findFeature(String id) {
        return Optional.ofNullable(featureIndex.get(id));
    }
}
