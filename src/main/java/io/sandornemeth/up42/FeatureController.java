package io.sandornemeth.up42;

import io.sandornemeth.up42.model.Feature;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8_VALUE;

/**
 * HTTP API for features.
 */
@RestController
@RequestMapping(produces = APPLICATION_JSON_UTF8_VALUE)
public class FeatureController {

    private FeatureRepository featureRepository;

    public FeatureController(FeatureRepository featureRepository) {
        this.featureRepository = featureRepository;
    }

    @GetMapping(value = "/features")
    public List<FeatureResponse> listFeatures() {
        return featureRepository.findAll().stream()
                .map(FeatureResponse::new)
                .collect(toList());
    }

    @GetMapping(value = "/features/{featureId}")
    public ResponseEntity<FeatureResponse> getFeature(@PathVariable String featureId) {
        return featureRepository.findFeature(featureId)
                .map(FeatureResponse::new)
                .map(ResponseEntity::ok)
                .orElse(ResponseEntity.notFound().build());
    }

    @GetMapping(value = "/features/{featureId}/quicklook")
    public ResponseEntity<byte[]> getQuickLook(@PathVariable String featureId) {
        return featureRepository.findFeature(featureId)
                .map(f -> f.getProperties().getQuicklook())
                .map(img -> ResponseEntity.ok().contentType(MediaType.IMAGE_PNG).body(img))
                .orElse(ResponseEntity.notFound().build());
    }

    /**
     * The response which the application delivers to its clients.
     */
    static class FeatureResponse {
        private String id;
        private long timestamp;
        private long beginViewingDate;
        private long endViewingDate;
        private String missionName;

        FeatureResponse(Feature feature) {
            this.id = feature.getProperties().getId();
            this.timestamp = feature.getProperties().getTimestamp();
            this.beginViewingDate = feature.getProperties().getAcquisition().getBeginViewingDate();
            this.endViewingDate = feature.getProperties().getAcquisition().getEndViewingDate();
            this.missionName = feature.getProperties().getAcquisition().getMissionName();
        }

        public String getId() {
            return id;
        }

        public long getTimestamp() {
            return timestamp;
        }

        public long getBeginViewingDate() {
            return beginViewingDate;
        }

        public long getEndViewingDate() {
            return endViewingDate;
        }

        public String getMissionName() {
            return missionName;
        }
    }

}
